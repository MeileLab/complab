# CompLaB-1.0.0

[CompLaB](https://bitbucket.org/MeileLab/complab/) is an open-source reactive transport modeling package developed for simulating the pore scale flow and transport with the genome-enabled metabolic modeling capability. CompLaB couples the Lattice Boltzmann method-based CFD library, [Palabos](https://palabos.unige.ch/), which enables advective and diffusive transport in an arbitrarily complex porous medium, with the linear programming solvers, [GLPK](https://www.gnu.org/software/glpk/) and [COBRApy](https://opencobra.github.io/cobrapy/), for flux balance analysis. It also provides a template for geochemical reactions and microbial dynamics to accommodate various user-specific needs.
 
# Contents

## docs: The directory containing the CompLaB user's manual.

## example

  - indimeshComparison
    : This folder includes 6 simulation subfolders showing the convergence of two codependent species to a specific ratio used for the CompLaB model verification. Each subfolder (sim1, sim2, ...) includes the following input files 
    - metabolic models of E.coli K12 (iJO1366_rGEM_c.xml) and S.enterica LT2 (iJO1366_rGEM_c.xml)
    - input domains (inputDomainx.dat)
    - CompLaB input file (CompLaB.xml)

  - scalability
    : It includes 4 subfolders each of which were used to measure the model performance using the following reaction solver
    - COBRApy (cpy)
    - GLPK (glpk)
    - Kinetics with the cellular automaton algorithm invoked (kns-ca)
    - Surrogate ANN model (surr)

## src
  : contains the CompLaB source codes
  
## defineKinetics.hh
  : A C++ template for kinetic rate expressions. Users can define rate expressions for biomass growth (bioR) and metabolite concentration (subsR) using biomass densities (B) and metabolite concentrations (C). All the function variables are vectors where the vector elements follow the order defined in CompLaB.xml input file. The expressions inside are used only when the reaction solver 'kinetics' is used in the input file, but this file has to be located in the main simulation folder with CompLaB.xml and compiled regardless of the reaction solver employed.

## surrogateModel.hh
  : A C++ template for direct coupling of surrogate models with CompLaB. It is up to users to relate local biomass density (B) and metabolite concentration (C) to pre-trained surrogate models. The code inside is the pre-trained artifitial neural network using the metabolic network of Geobacter metallireducens GS-15 (iAF987). 
  
# Getting help / Bug reports

If you think you have identified a bug or need help, please contact [Heewon Jung](hjung@cnu.ac.kr) or [Christof Meile](cmeile@uga.edu).

