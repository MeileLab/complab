/* This file is a part of the CompLaB program.
 *
 * The CompLaB softare is developed since 2022 by the University of Georgia
 * (United States) and Chungnam National University (South Korea).
 * 
 * Contact:
 * Heewon Jung
 * Department of Geological Sciences 
 * Chungnam National University
 * 99 Daehak-ro, Yuseong-gu
 * Daejeon 34134, South Korea
 * hjung@cnu.ac.kr
 *
 * The most recent release of CompLaB can be downloaded at 
 * https://bitbucket.org/MeileLab/complab/downloads/
 *
 * CompLaB is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// PyObject -> Vector (DOUBLE)
std::vector< std::vector< std::vector<double> > > getDoubleArray3D(PyObject* inputArg) {
    std::vector< std::vector< std::vector<double> > > data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT2 = 0; iT2 < PyList_Size(inputArg); ++iT2) {
            PyObject *value2 = PyList_GetItem(inputArg, iT2);
            if (PyList_Check(value2)) {
                data[iT2] = std::vector< std::vector<double> > (PyList_Size(value2));
                for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(value2); ++iT0) {
                    PyObject *value0 = PyList_GetItem(value2, iT0);
                    if (PyList_Check(value0)) {
                        data[iT2][iT0] = std::vector<double> (PyList_Size(value0));
                        for(Py_ssize_t iT1 = 0; iT1 < PyList_Size(value0); ++iT1) {
                            PyObject *value1 = PyList_GetItem(value0, iT1);
                            if (!PyList_Check(value1)) {
                                data[iT2][iT0][iT1] = PyFloat_AsDouble(value1);
                            }
                            else {
                                pcout << "3rd dimension of the stoichimetric matrix (value1) is still a list" << std::endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                    }
                    else {
                        pcout << "2nd dimension of the stoichimetric matrix (value0) is not a list!" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
            }
            else {
                pcout << "1st dimension of the stoichimetric matrix (value2) is not a list!" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getStoichimetricMatrix Error" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

std::vector< std::vector<double> > getDoubleArray2D(PyObject* inputArg) {
    std::vector< std::vector<double> > data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(inputArg); ++iT0) {
            PyObject *value0 = PyList_GetItem(inputArg, iT0);
            if (PyList_Check(value0)) {
                data[iT0] = std::vector<double> (PyList_Size(value0));
                for(Py_ssize_t iT1 = 0; iT1 < PyList_Size(value0); ++iT1) {
                    PyObject *value1 = PyList_GetItem(value0, iT1);
                    if (!PyList_Check(value1)) {
                        data[iT0][iT1]= PyFloat_AsDouble(value1);
                    }
                    else {
                        pcout << "2nd dimension of getDoubleArray2D (value1) is still a list" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
            }
            else {
                pcout << "1st dimension of getDoubleArray2D (value0) is not a list!" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getDoubleArray2D Error" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

std::vector<double> getDoubleArray1D(PyObject* inputArg) {
    std::vector<double> data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(inputArg); ++iT0) {
            PyObject *value0 = PyList_GetItem(inputArg, iT0);
            if (!PyList_Check(value0)) {
                data[iT0] = PyFloat_AsDouble(value0);
            }
            else {
                pcout << "getDoubleArray1D ERROR: 2nd dimension of the array shouldn't be a list" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getDoubleArray1D ERROR: 1st dimension of the array is not a list!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

std::vector<PyObject *> getPyObject1D(PyObject* inputArg) {
    std::vector<PyObject *> data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(inputArg); ++iT0) {
            PyObject *value0 = PyList_GetItem(inputArg, iT0);
            if (!PyList_Check(value0)) {
                data[iT0] = value0;
            }
            else {
                pcout << "getPyObject1D ERROR: 2nd dimension of the array shouldn't be a list" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getPyObject1D ERROR: 1st dimension of the array is not a list!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

// PyObject -> Vector (INT)
std::vector< std::vector<int> > getIntArray2D(PyObject* inputArg) {
    std::vector< std::vector<int> > data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(inputArg); ++iT0) {
            PyObject *value0 = PyList_GetItem(inputArg, iT0);
            if (PyList_Check(value0)) {
                data[iT0] = std::vector<int> (PyList_Size(value0));
                for(Py_ssize_t iT1 = 0; iT1 < PyList_Size(value0); ++iT1) {
                    PyObject *value1 = PyList_GetItem(value0, iT1);
                    if (!PyList_Check(value1)) {
                        data[iT0][iT1]= PyLong_AsLong(value1);
                    }
                    else {
                        pcout << "2nd dimension of getIntArray2D (value1) is still a list" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
            }
            else {
                pcout << "1st dimension of getIntArray2D (value0) is not a list!" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getIntArray2D Error" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

std::vector<int> getIntArray1D(PyObject* inputArg) {
    std::vector<int> data(PyList_Size(inputArg));
    if (PyList_Check(inputArg)) {
        for(Py_ssize_t iT0 = 0; iT0 < PyList_Size(inputArg); ++iT0) {
            PyObject *value0 = PyList_GetItem(inputArg, iT0);
            if (!PyList_Check(value0)) {
                data[iT0] = PyLong_AsLong(value0);
            }
            else {
                pcout << "getIntArray1D ERROR: 2nd dimension of the array shouldn't be a list" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
    else {
        pcout << "getIntArray1D ERROR: 1st dimension of the array is not a list!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return data;
}

void check_model(char *fileName, std::vector<PyObject *> model, int x, int y)
{
    PyObject *pName = PyUnicode_FromString(fileName);
    PyObject *pModule = PyImport_Import(pName); // load the script named pName
    Py_DECREF(pName); // decrement the reference counting number
    if (pModule != NULL) {
        PyObject *pFunc = PyObject_GetAttrString(pModule, "check_model");
        /* pFunc is a new reference */
        if (pFunc && PyCallable_Check(pFunc)) {
            int len = model.size();
            PyObject *pArgs = PyTuple_New(len+2); // Return a new tuple object of size len, or NULL on failure.
            for (int i = 0; i < len; ++i) {
                PyTuple_SetItem(pArgs, i, model[i]);
            }
            PyTuple_SetItem(pArgs, len, PyLong_FromLong(x));
            PyTuple_SetItem(pArgs, len+1, PyLong_FromLong(y));
            PyObject_CallObject(pFunc, pArgs);
            Py_DECREF(pArgs);
        }
        Py_XDECREF(pFunc); // use xdecref instead of decref if you are not sure whether the argument is NULL or not.
        Py_DECREF(pModule);
    }
}

int prep_cobrapy(char *pyFileName, char *src_path, std::vector<PyObject*> &vec_model, std::vector<std::vector<T>> S, std::vector<std::vector<T>> c, std::vector<std::vector<T>> lb,
                  std::vector<std::vector<T>> ub, std::vector<int> vec_nrxns, std::vector<int> vec_nmets, std::vector<plint> vec_objLoc)
{
    pcout << "Feeding metabolic models to COBRApy ..." << std::endl;
    char funName[] = "feed_cobraModel"; // python function name in fileName
    plint num_of_microbes = S.size();
    PyObject *sysPath = PySys_GetObject((char*)"path");
    PyList_Append(sysPath, PyUnicode_FromString(src_path));
    PyObject *pName = PyUnicode_FromString("complab_cobrapy");
    PyObject *pModule = PyImport_Import(pName); // load the script named pName
    Py_DECREF(pName); // decrement the reference counting number
    int erck = 0;
    for (plint j = 0; j < num_of_microbes; ++j) {
        int nmets = vec_nmets[j];
        int nrxns = vec_nrxns[j];
        int argsize = (nmets+3)*nrxns+2;
        std::vector<double> args(argsize);
        // std::vector<const char *> args(argsize);
        args[0]=double(nmets);
        args[1]=double(nrxns);
        for (int i = 0; i < nmets*nrxns; ++i) {
            args[2+i] = S[j][i];
        }
        for (int i = 0; i < nrxns; ++i) {
            args[2+nmets*nrxns+i] = lb[j][i];
            args[2+(nmets+1)*nrxns+i] = ub[j][i];
            args[2+(nmets+2)*nrxns+i] = c[j][i];
        }
        // args[0]=std::to_string(nmets).c_str();
        // args[1]=std::to_string(nrxns).c_str();
        // for (int i = 0; i < nmets*nrxns; ++i) {
        //     args[2+i] = std::to_string(S[j][i]).c_str();
        // }
        // for (int i = 0; i < nrxns; ++i) {
        //     args[2+nmets*nrxns+i] = std::to_string(lb[j][i]).c_str();
        //     args[2+(nmets+1)*nrxns+i] = std::to_string(ub[j][i]).c_str();
        //     args[2+(nmets+2)*nrxns+i] = std::to_string(c[j][i]).c_str();
        // }

        // std::cout << args[0] <<" "<< args[1] << std::endl;
        if (pModule != NULL) {
            PyObject *pFunc = PyObject_GetAttrString(pModule, funName);
            if (pFunc && PyCallable_Check(pFunc)) {
                PyObject *pArgs = PyTuple_New(argsize); // Return a new tuple object of size len, or NULL on failure.
                for (int i = 0; i < argsize; ++i) {
                    // PyObject *pValue = PyUnicode_FromString(args[i]);
                    PyObject *pValue = PyFloat_FromDouble(args[i]);
                    if (!pValue) {
                        Py_DECREF(pArgs);
                        Py_DECREF(pModule);
                        fprintf(stderr, "Cannot convert argument\n");
                        erck = 1;
                    }
                    /* pValue reference stolen here: */
                    PyTuple_SetItem(pArgs, i, pValue);
                }
                vec_model[j]=PyObject_CallObject(pFunc, pArgs);
                Py_DECREF(pArgs);
                if (vec_model[j] == NULL) {
                    Py_DECREF(pFunc);
                    Py_DECREF(pModule);
                    PyErr_Print();
                    fprintf(stderr,"Call failed\n");
                    erck = 1;
                }
            }
            else {
                if (PyErr_Occurred())
                    PyErr_Print();
                fprintf(stderr, "Cannot find function \"%s\"\n", funName);
                erck = 1;
            }
            Py_XDECREF(pFunc); // use xdecref instead of decref if you are not sure whether the argument is NULL or not.
            Py_DECREF(pModule);
        }
        else {
            PyErr_Print();
            fprintf(stderr, "Failed to load \"%s\"\n", pyFileName);
            erck = 1;
        }
    }
    return erck;
}

int load_metabolic_model( char *fileName, char *funName, std::vector<const char *> args, std::vector< std::vector< std::vector<double> > > &S, std::vector< std::vector<double> > &b,
                           std::vector< std::vector<double> > &c, std::vector< std::vector<double> > &lb, std::vector< std::vector<double> > &ub, int microbesNum )
{
    // args = [ workDir. input_dirname, mmFile#, mmFile#, exsubs#, exsubs#, num_of_microbes]

    // check if a file extension is included in the input argument.
    // if the version is python2 instead of python3, PyUnicode_FromString should be replaced to PyString_FromString
    char *pos = strrchr(fileName, '.');
    if ( pos ) { // if there is an extension
        int len = pos-fileName;
        char substr[len];
        memcpy(substr, &fileName[0],len);
        substr[len] = '\0';
        fileName = substr;
    }

    PyObject *sysPath = PySys_GetObject((char*)"path");
    PyList_Append(sysPath, PyUnicode_FromString("."));

    PyObject *pName = PyUnicode_FromString(fileName);
    PyObject *pModule = PyImport_Import(pName); // load the script named pName
    Py_DECREF(pName); // decrement the reference counting number
    if (pModule != NULL) {
        PyObject *pFunc = PyObject_GetAttrString(pModule, funName);
        if (pFunc && PyCallable_Check(pFunc)) {
            int len = args.size();
            PyObject *pArgs = PyTuple_New(len+1); // Return a new tuple object of size len, or NULL on failure.
            for (int i = 0; i < len; ++i) {
                PyObject *pValue = PyUnicode_FromString(args[i]);
                if (!pValue) {
                    Py_DECREF(pArgs);
                    Py_DECREF(pModule);
                    fprintf(stderr, "Cannot convert argument\n");
                }
                /* pValue reference stolen here: */
                PyTuple_SetItem(pArgs, i, pValue);
            }
            PyTuple_SetItem(pArgs, len, PyLong_FromLong(microbesNum));
            // run the python function
            PyObject *pValue = PyObject_CallObject(pFunc, pArgs);
            Py_DECREF(pArgs);
            if (pValue != NULL) {
                S = getDoubleArray3D(PyList_GetItem(pValue, 0));
                b = getDoubleArray2D(PyList_GetItem(pValue, 1));
                c = getDoubleArray2D(PyList_GetItem(pValue, 2));
                lb = getDoubleArray2D(PyList_GetItem(pValue, 3));
                ub = getDoubleArray2D(PyList_GetItem(pValue, 4));
                // model = getPyObject1D(PyList_GetItem(pValue, 5));
                Py_DECREF(pValue); // this might have to be commented out to save memory from being destroyed
            }
            else {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "Cannot find function \"%s\"\n", funName);
        }
        Py_XDECREF(pFunc); // use xdecref instead of decref if you are not sure whether the argument is NULL or not.
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", fileName);
    }

    return 0;
}

void optimize_cobrapy( char *fileName, std::vector<double> args, std::vector<PyObject *> model, std::vector<double> &grate, std::vector< std::vector<double> > &flux )
{

    PyObject *pName = PyUnicode_FromString(fileName);
    PyObject *pModule = PyImport_Import(pName); // load the script named pName
    Py_DECREF(pName); // decrement the reference counting number
    if (pModule != NULL) {
        PyObject *pFunc = PyObject_GetAttrString(pModule, "run_FBA");
        if (pFunc && PyCallable_Check(pFunc)) {
            int len = args.size();
            int num_of_microbes = std::round(args[0]);
            // need to add models to python argument. So the total length of the argument vector is len+num_of_microbes
            PyObject *pArgs = PyTuple_New(len+num_of_microbes); // Return a new tuple object of size len, or NULL on failure.
            for (int i = 0; i < len; ++i) {
            	PyObject *pValue = PyFloat_FromDouble(args[i]);
                if (!pValue) {
                    Py_DECREF(pArgs);
                    Py_DECREF(pModule);
                    fprintf(stderr, "Cannot convert argument (optimize_cobrapy)\n");
                }
                /* pValue reference stolen here: */
            	PyTuple_SetItem(pArgs, i, pValue);
            }
            for (int i = 0; i < num_of_microbes; ++i) {
            	/* pValue reference stolen here: */
            	PyTuple_SetItem(pArgs, len+i, model[i]);
            }
            // run the python function
            PyObject *pValue = PyObject_CallObject(pFunc, pArgs);
            // Py_DECREF(pArgs); // uncommenting it destroys all python variables in args vector including model, which leads to unexpected simulation results
            if (pValue != NULL) {
                grate = getDoubleArray1D(PyList_GetItem(pValue, 0));
                flux = getDoubleArray2D(PyList_GetItem(pValue, 1));
                Py_DECREF(pValue);
            }
            else {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "Cannot find function \"%s\"\n", "run_FBA");
        }
        Py_XDECREF(pFunc); // use xdecref instead of decref if you are not sure whether the argument is NULL or not.
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", fileName);
    }
}
