"""
/* This file is a part of the CompLaB program.
 *
 * The CompLaB softare is developed since 2022 by the University of Georgia
 * (United States) and Chungnam National University (South Korea).
 *
 * Contact:
 * Heewon Jung
 * Department of Geological Sciences 
 * Chungnam National University
 * 99 Daehak-ro, Yuseong-gu
 * Daejeon 34134, South Korea
 * hjung@cnu.ac.kr
 *
 * The most recent release of CompLaB can be downloaded at 
 * https://bitbucket.org/MeileLab/complab/downloads/
 *
 * CompLaB is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
"""

import os
import sys
from os.path import join
import copy
import cobra
import math
import numpy as np
from cobra import Model, Reaction, Metabolite

def feed_cobraModel(*args):
    # args = [ nmets, nrxns, S#1..., lb#1..., ub#1..., c#1... ]
    nmets = int(args[0])
    nrxns = int(args[1])
    S = []; lb = []; ub = []; c = [];
    for i in range (0, nmets*nrxns):
        S.append(args[i+2])
    for i in range (0, nrxns):
        lb.append(args[i+nmets*nrxns+2])
        ub.append(args[i+(nmets+1)*nrxns+2])
        c.append(args[i+(nmets+2)*nrxns+2])

    model = Model('0')
    for j in range (0, nrxns):
        reaction = Reaction(str(j))
        reaction.lower_bound = lb[j]
        reaction.upper_bound = ub[j]
        for i in range (0, nmets):
            metabolite = Metabolite(str(i))
            reaction.add_metabolites({metabolite:S[i*nrxns+j]})
        model.add_reactions([reaction])
        model.reactions[j].objective_coefficient=c[j]

    return [model]

def run_FBA(*args):
    # args = [number of microbes, number of substrates for each microbe, location of metabolites in S ..., lower bounds ..., metabolic models ...]
    num_of_microbes = int(args[0])
    total_length = len(args)
    num_of_subs = [None] * num_of_microbes
    total_num_of_subs = 0
    for i in range (0,num_of_microbes):
        num_of_subs[i] = int(args[i+1])
        total_num_of_subs += num_of_subs[i]

    model = [None] * num_of_microbes
    loc = [None] * num_of_microbes
    lb = [None] * num_of_microbes
    ub = [None] * num_of_microbes
    prev_subsNum = 0
    for i in range (0,num_of_microbes):
        current_subsNum = num_of_subs[i]
        if i > 0:
            prev_subsNum += num_of_subs[i-1]
        loc[i] = [None]*current_subsNum
        lb[i] = [None]*current_subsNum
        ub[i] = [None]*current_subsNum
        model[i] = args[total_length-num_of_microbes+i][0]
        for j in range (0,current_subsNum):
            loc[i][j] = int(args[(1+num_of_microbes)+prev_subsNum+j])
            lb[i][j] = args[(1+num_of_microbes+total_num_of_subs)+prev_subsNum+j]
            ub[i][j] = args[(1+num_of_microbes+total_num_of_subs*2)+prev_subsNum+j]

    for i in range (0, num_of_microbes):
        for j in range (0,num_of_subs[i]):
            model[i].reactions[loc[i][j]].lower_bound = lb[i][j]
            model[i].reactions[loc[i][j]].upper_bound = ub[i][j]

    result = [None]*num_of_microbes
    grate = [None]*num_of_microbes
    flux = [None]*num_of_microbes
    
    for i in range (0, num_of_microbes):
        result[i] = model[i].optimize()
        grate[i] = result[i].objective_value
        flux[i] = [None]*num_of_subs[i]
        flag = 0
        for j in range (0, num_of_subs[i]):
            flux[i][j]= result[i].fluxes[loc[i][j]]

    return [grate,flux]
