#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 21:54:21 2021

@author: hjung
"""

import os
import sys
import cobra
import numpy as np
from xml.dom import minidom

os.chdir(os.getcwd())

fin=sys.argv[1]
name=fin.split('.', 1)[0]

if fin.endswith('.xml'):
    model = cobra.io.read_sbml_model(fin)
elif fin.endswith('.mat'):
    model = cobra.io.load_matlab_model(fin)
elif fin.endswith('.json'):
    model = cobra.io.load_json_model(fin)
else:
    print(fin)
    sys.exit('ERROR: only xml, mat, json files are supported for now')
S=cobra.util.array.create_stoichiometric_matrix(model)
lb=[]; ub=[]; c=[]; b=[];
for j in range (0, len(model.reactions)):
    lb.append(model.reactions[j].lower_bound)
    ub.append(model.reactions[j].upper_bound)
    objcoef = model.reactions[j].objective_coefficient;
    c.append(objcoef)
    if objcoef==1:
        objloc=j
    b.append(0)

nmet = len(S)
nrxn = len(lb)

# create XML file
root = minidom.Document()

model_elm = root.createElement('Metabolic_Model')
root.appendChild(model_elm)

child_name = root.createElement('name')
child_met = root.createElement('nmet')
child_rxn = root.createElement('nrxn')
child_S = root.createElement('S')
child_b = root.createElement('b')
child_c = root.createElement('c')
child_lb = root.createElement('lb')
child_ub = root.createElement('ub')
child_objloc = root.createElement('objLoc')

child_name.appendChild(root.createTextNode(name))
model_elm.appendChild(child_name)

child_met.appendChild(root.createTextNode(str(nmet)))
model_elm.appendChild(child_met)

child_rxn.appendChild(root.createTextNode(str(nrxn)))
model_elm.appendChild(child_rxn)

child_objloc.appendChild(root.createTextNode(str(objloc)))
model_elm.appendChild(child_objloc)

# S=np.transpose(S)
S=S.flatten() # loop over metabolites and then reactions
str_S = ["%g" % x for x in S]
str_S=" ".join(str_S)
str_S = " " + str_S + " "
child_S.appendChild(root.createTextNode(str_S))
model_elm.appendChild(child_S)

str_b = ["%g" % x for x in b]
str_b=" ".join(str_b)
str_b = " " + str_b + " "
child_b.appendChild(root.createTextNode(str_b))
model_elm.appendChild(child_b)

str_c = ["%g" % x for x in c]
str_c=" ".join(str_c)
str_c = " " + str_c + " "
child_c.appendChild(root.createTextNode(str_c))
model_elm.appendChild(child_c)

str_lb = ["%g" % x for x in lb]
str_lb=" ".join(str_lb)
str_lb = " " + str_lb + " "
child_lb.appendChild(root.createTextNode(str_lb))
model_elm.appendChild(child_lb)

str_ub = ["%g" % x for x in ub]
str_ub=" ".join(str_ub)
str_ub = " " + str_ub + " "
child_ub.appendChild(root.createTextNode(str_ub))
model_elm.appendChild(child_ub)

xml_str = root.toprettyxml(indent ="\t")

save_path_file = name+".xml"

with open(save_path_file, "w") as f:
    f.write(xml_str)

