#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 20:37:19 2020

@author: heewon
"""

import csv
import copy
import numpy as np

b1=[]
with open('inputDomain.dat', newline='') as csvfile:
     csv1 = csv.reader(csvfile, delimiter=' ', quotechar='|')
     for row in csv1:
         b1.append(float(row[0]))

for i in range (0, len(b1)):
    if b1[i] == 2:
        b1[i]=0
    elif b1[i]==3:
        b1[i]=1
    elif b1[i]==4:
        b1[i]=2

b2=copy.deepcopy(b1)
for i in range (0, len(b2)):
    if b2[i] == 8:
        b2[i]=1
    else:
        b2[i]=0

for i in range (0, len(b1)):
    if b1[i] == 8:
        b1[i]=0

outF = open("b1.dat", "w")
for line in b1:
    outF.write(str(line))
    outF.write("\n")
outF.close()

outF = open("b2.dat", "w")
for line in b2:
    outF.write(str(line))
    outF.write("\n")
outF.close()