clear

cd /Users/heewon/work/gitRepo/localRepo/jung_lbfba/input

dom1=zeros(27,25);
dom2=dom1;

randx=randi([2 26],1,99); randy=randi([1 25],1,99);
for i=1:99
    tmpx=randx(i); tmpy=randy(i);
    dom1(tmpx,tmpy)=dom1(tmpx,tmpy)+1;
end
randx=randi([2 26],1,1); randy=randi([1 25],1,1);
dom2(randx,randy)=dom2(randx,randy)+1;

for i=1:27
    for j=1:25
        f1((i-1)*25+j,1)=dom1(i,j);
        f2((i-1)*25+j,1)=dom2(i,j);
    end
end
f1=int8(f1); f2=int8(f2);
csvwrite('caseA1_1.dat',f1)
csvwrite('caseA1_2.dat',f2)