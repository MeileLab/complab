#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 18:32:06 2020

@author: heewon
"""

import numpy as np
import random
import matplotlib.pyplot as plt

dom1 = np.zeros((25,25))

for i in range (0,99):
    x = random.randint(0, 24)
    y = random.randint(0, 24)
    dom1[x][y]=dom1[x][y]+1
    
while np.max(dom1)<6
x = random.randint(0, 24)
y = random.randint(0, 24)
if dom1[x][y] == 0:
    dom1[x][y]=6
    
plt.imshow(dom1)
plt.xlabel('x')

dom2=dom1+2
v=[None]*25*25
for i in range (0,25):
    for j in range (0,25):
        v[i*25+j]=int(dom2[j][i])

outF = open("inputDomain.dat", "w")
for line in v:
    outF.write(str(line))
    outF.write("\n")
outF.close()

