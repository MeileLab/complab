% ecoli(produce acetate)
model1=load('Cb_iJO1366_rGEM.mat');
model1=model1.model;
loc1 = [7 56 419 422];
model1.lb(loc1(1))=-15;
model1.lb(loc1(2))=-0.1;
model1.lb(loc1(3))=-8;
model1.lb(loc1(4))=-2.92;
FBAsolution1 = optimizeCbModel(model1,'max');

% S.enterica(produce methionine)
model2 = load('Cb_iRR1083_rGEM.mat');
model2 = model2.model;
loc2 = [72 105 100];
model2.lb(loc2(1))=-15;
model2.lb(loc2(2))=-0.1;
model2.lb(loc2(3))=-8;
FBAsolution2 = optimizeCbModel(model2,'max');

FBAsolution1.v(loc1)
FBAsolution2.v(loc2)

%%
% ecoli(produce acetate)
model3=load('Cb_iJO1366_GEM.mat');
model3=model3.model;
loc3=[36 254 232 212];
model3.lb(loc3(1))=-15;
model3.lb(loc3(2))=-0.1;
model3.lb(loc3(3))=-8;
model3.lb(loc3(4))=-2.92;
FBAsolution3 = optimizeCbModel(model3,'max');

% S.enterica(produce methionine)
model4 = load('Cb_iRR1083_GEM.mat');
model4 = model4.model;
loc4 = [666 872 853];
model4.lb(loc4(1))=-15;
model4.lb(loc4(2))=-0.1;
model4.lb(loc4(3))=-8;
FBAsolution4 = optimizeCbModel(model4,'max');

FBAsolution3.v(loc3)
FBAsolution4.v(loc4)

%%
% ecoli(produce acetate)
model5 = readCbModel('iJO1366_Orth2011.xml');
loc5 = [36 252 230 210];
model5.lb(loc5(1))=-15;
model5.lb(loc5(2))=-0.1;
model5.lb(loc5(3))=-8;
model5.lb(loc5(4))=-2.92;
FBAsolution5 = optimizeCbModel(model5,'max');

% s.enterica(produce methionine)
model6 = readCbModel('iRR1083_Ragh2009.xml');
loc6 = [363 501 486];
model6.lb(loc6(1))=-15;
model6.lb(loc6(2))=-0.1;
model6.lb(loc6(3))=0;
FBAsolution6 = optimizeCbModel(model6,'max');
FBAsolution6.f

FBAsolution5.v(loc5)
FBAsolution6.v(loc6)

